import os
import sqlite3
import datetime


def firefox_hist():
    profiles_dir = os.path.join(os.getenv("appdata"), "Mozilla\\Firefox\\Profiles")
    firefox_profiles = os.listdir(profiles_dir)
    lines = []
    for profile in firefox_profiles:
        database_file = os.path.join(profiles_dir, profile, "places.sqlite")
        db = sqlite3.connect(database_file)
        cursor = db.cursor()
        cursor.execute("select p.url, p.visit_count, h.last_visit_time from moz_places as p join (select place_id, max(visit_date) as last_visit_time from moz_historyvisits group by place_id) as h on p.id = h.place_id;")
        results = cursor.fetchall()
        cursor2 = db.cursor()
        cursor2.execute("SELECT * from moz_historyvisits")
        for url, count, last_visit_time in results:
            visit_time = str(datetime.datetime(1970, 1, 1) + datetime.timedelta(microseconds=last_visit_time))  # the -389 is to get the good year
            visit_time = visit_time[:-7]
            lines.append(str(visit_time) + "," + url + " : " + str(count))
    return "\n".join(sorted(lines, key=lambda x: int(x.split(" : ")[-1]), reverse=True))


def main(filename):
    with open(filename, "w") as f:
        f.write(firefox_hist())


if __name__ == "__main__":
    main("firefoxhistorydumped.txt")
