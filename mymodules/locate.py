"""
recursive search for a string in file names & directories
"""

import os
import sys


def smarter(path, to_find):
    for dirpath, dirnames, filenames in os.walk(path):
        for e in dirnames:
            if to_find.lower() in e.lower():
                print(os.path.join(dirpath, e))
        for e in filenames:
            if to_find.lower() in e.lower():
                print(os.path.join(dirpath, e))


if __name__ == "__main__":
    print(sys.argv)
    if len(sys.argv) != 3:
        print(f"Recursive search \n USAGE : {sys.argv[0]} [PATH] [filename]")
        sys.exit()
    smarter(sys.argv[1], sys.argv[2])
