"""
A module that dump chrome history into a simple txt file
"""
import sqlite3
import os
import datetime


def history_chrome():
    chromefiles_path = os.getenv('localappdata') + '\\Google\\Chrome\\User Data\\Default\\'
    data_path = os.path.expanduser(chromefiles_path)
    history_db = os.path.join(data_path, 'history')
    c = sqlite3.connect(history_db)
    cursor = c.cursor()
    select_statement = "SELECT url, visit_count, last_visit_time FROM urls;"
    cursor.execute(select_statement)
    results = cursor.fetchall()
    lines = []
    for url, count, last_visit_time in results:
        visit_time = str(datetime.datetime(1970 - 369, 1, 1) + datetime.timedelta(microseconds=last_visit_time))  # the -389 is to get the good year
        visit_time = visit_time[:-7]
        lines.append(str(visit_time) + "," + url + " : " + str(count))
    return "\n".join(sorted(lines, key=lambda x: int(x.split(" : ")[-1]), reverse=True))


def main(filename):
    with open(filename, "w") as f:
        f.write(history_chrome())


if __name__ == "__main__":
    os.system("taskkill /F /IM chrome* /T")  # to be sure the database is not locked
    main("chromehistorydumped.txt")
