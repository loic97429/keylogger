"""
make a json map of the computer files
"""

import os
import json
from getpass import getuser
from threading import Thread


def smarter(path):
    results = {}
    for dirpath, dirnames, filenames in os.walk(path):
        if len(filenames) == 0:
            continue
        parts = dirpath.replace(path, "").split(os.sep)
        curr = results
        for index, item in enumerate(parts):
            if item not in curr:
                curr[item] = {}
            curr = curr[item]
        for f in filenames:
            curr[f] = f
    return results


def saveIntoJson(info, location):
    with open(location, "w") as f:
        json.dump(info, f, indent=4)


if __name__ == "__main__":
    t1 = Thread(target=lambda: saveIntoJson(smarter(f"c:\\users\\{getuser()}\\Documents"), "documentsmap.json"))
    t1.start()
    t2 = Thread(target=lambda: saveIntoJson(smarter(f"c:\\users\\{getuser()}\\Pictures"), "picturesmap.json"))
    t2.start()
    t3 = Thread(target=lambda: saveIntoJson(smarter(f"c:\\users\\{getuser()}\\Videos"), "videosmap.json"))
    t3.start()
    t4 = Thread(target=lambda: saveIntoJson(smarter(f"c:\\users\\{getuser()}\\Desktop"), "desktopmap.json"))
    t4.start()
    t5 = Thread(target=lambda: saveIntoJson(smarter(f"c:\\users\\{getuser()}\\Downloads"), "downloadsmap.json"))
    t5.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
